import {Component} from '@angular/core';
import {SQLite, SQLiteObject} from '@ionic-native/sqlite/ngx';
import {MenuController, Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import {Storage} from '@ionic/storage';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import {TranslateService} from "@ngx-translate/core";
@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent {
    public database: any;
    public isUser = false;
    public typez = 'overlay';
    public url = this.router.url;
    public color: any;
    public appPages = [
        {
            title: 'Home',
            url: '/home',
            icon: 'home'
        },
        {
            title: 'List',
            url: '/list',
            icon: 'list'
        },
        {
            title: 'login',
            url: '/login',
            icon: 'login'
        }
    ];

    constructor(
        private router: Router,
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private sqlite: SQLite,
        private menu: MenuController,
        private storage: Storage,
        private translate: TranslateService,
        private screen: ScreenOrientation,
    ) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(async () => {

            const lang = await this.storage.get('lang');

            this.translate.setDefaultLang(lang ? lang : 'ru');

            this.screen.lock(this.screen.ORIENTATIONS.LANDSCAPE);
            this.color = setInterval(() => { this.getColor(); }, 1000);
            this.menu.close('first');
            this.statusBar.hide();
            // this.router.navigate(['/coupons']);
            this.splashScreen.hide();
            this.sqlite.create({
                name: 'data.db',
                location: 'default'
            }).then((db: SQLiteObject) => {
                this.database = db;
                // db.executeSql('DROP TABLE user', []);
                db.executeSql('CREATE TABLE IF NOT EXISTS cards (id INTEGER PRIMARY KEY AUTOINCREMENT, number TEXT, cvv TEXT, date TEXT)', []).then((data) => {
                    console.log('Table cards created');
                }, (error) => {
                    console.log('Error! cards' + error);
                });
                db.executeSql('CREATE TABLE IF NOT EXISTS user (id INTEGER PRIMARY KEY AUTOINCREMENT, tel TEXT, uid INTEGER, name TEXT, coupon_status TEXT, token TEXT, cache TEXT, email TEXT)', []).then((data) => {
                    console.log('Table user created. Initialize');
                    db.executeSql('SELECT * FROM user', []).then((data) => {
                        if (data.rows.length > 0) {
                            this.router.navigate(['/player']);
                            this.menu.enable(true, 'first');
                        } else {
                            this.router.navigate(['/login']);
                        }
                    }, (error) => {
                    });
                }, (error) => {
                });
            });
        });
    }

    exit() {
        this.database.executeSql('DELETE FROM user', []);
        this.database.executeSql('DELETE FROM cards', []);
        this.router.navigate(['/login']);
        this.menu.enable(false, 'first');
    }

    closeMenu() {
        this.menu.close('first');
    }
    getColor() {
        this.storage.get('color').then((val) => {
            this.color = val;
        });
    }

    // checkSession(token) {
    //     this.http.get('http://185.178.44.126/api/sessions', {}, {'Authorization': 'Bearer ' + token})
    //         .then(data => {
    //             data.data = JSON.parse(data.data);
    //             //alert("Ответ сервера: message = " + JSON.stringify(data.data.message) + " " + JSON.stringify(data.data));
    //             if (data.data.success == true) {
    //                 //alert("success");
    //
    //                 if (Number(data.data.data[0].done) == 0) {
    //                     this.nav.setRoot(SessionPage);
    //                 } else {
    //                     this.nav.setRoot(MapPage);
    //                 }
    //             }
    //         }).catch(error => {
    //         console.log('FUCK' + JSON.stringify(error.status));
    //         console.log('FUCK' + JSON.stringify(error.error)); // error message as string
    //         console.log('FUCK' + JSON.stringify(error.headers));
    //     });
    // }
}
