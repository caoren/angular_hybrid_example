import { Component, OnInit } from '@angular/core';
import {SQLite, SQLiteObject} from '@ionic-native/sqlite/ngx';
import { HTTP } from '@ionic-native/http/ngx';
@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {
    public database: any;
    public uid: string;
    public coupons: any[];
  constructor(public sqlite: SQLite, private http: HTTP) {
      this.sqlite.create({ name: 'data.db', location: 'default' }).then((db: SQLiteObject) => {
          console.log('opened info!');
          this.database = db;
          this.database.executeSql('SELECT * FROM user', []).then((data) => {
              if (data.rows.length > 0) {
                  this.uid = data.rows.item(0).uid;
              }
          }, (error) => {
              alert('error user: ' + JSON.stringify(error.err));
              console.log('ERROR: ' + JSON.stringify(error.err));
          });
      }, (error) => {
          console.log('ERROR: ', error);
          // alert(error);
      });

      this.getCouponsUser();
  }

    getCouponsUser() {
            this.http.get('http://sound.aumagency.ru/api/user/getCoupons?id=' + this.uid, {}, {})
                .then(data => {
                  this.coupons = JSON.parse(data.data).data;
                })
                .catch(error => {
                    // console.log(error);
                    console.log(error.status);
                    console.log(error.error); // error message as string
                    console.log(error.headers);
                });
    }

  ngOnInit() {
      this.getCouponsUser();
  }

    ionViewWillEnter() {
        this.getCouponsUser();
    }

}
