import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {PlayerPage, FormatTimePipe, OrderBy} from './player.page';
import {HTTP} from '@ionic-native/http/ngx';
import {DragulaModule} from "ng2-dragula";
import {TranslateModule} from "@ngx-translate/core";

const routes: Routes = [
    {
        path: '',
        component: PlayerPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        DragulaModule.forRoot(),
        TranslateModule,
        RouterModule.forChild(routes)
    ],
    declarations: [PlayerPage, FormatTimePipe, OrderBy],
    providers: [HTTP]
})
export class PlayerPageModule {
}
