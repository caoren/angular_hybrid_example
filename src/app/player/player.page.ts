import {Component, OnInit, Pipe, PipeTransform} from '@angular/core';
import {HTTP} from '@ionic-native/http/ngx';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import {AlertController, MenuController, LoadingController} from '@ionic/angular';
import {of, Subscription} from 'rxjs';
import {Storage} from '@ionic/storage';
import {DragulaService} from "ng2-dragula";
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: 'app-player',
    templateUrl: './player.page.html',
    styleUrls: ['./player.page.scss'],
})
export class PlayerPage {
    public playing = [];
    public osc = [];
    public freq = [];
    public play = false;
    public context = new AudioContext();
    public STEP_CONSTANT = Math.pow(2.0, 1.0 / 12.0);
    public i = '';
    public selectedCourse = '';
    // public progress = '';
    public time = '';
    public cat_id: any;
    public sounds: any[];
    public settings: any[];
    public playlists: any[];
    public courses: any[];
    public coursesFilter: any[];
    public categories: any[];
    public currSound: any;
    public currSoundId: any[];
    selectedSounds: any[] = [];
    public soundlist: any[];
    public filtredSounds: any[];
    public countDown;
    public counter = 0;
    public progress = 0;
    public tick = 1000;
    public color: any;
    public upack: any;
    public duration = 0;
    public observableVar: Subscription;
    user: any = {};
    progressTimeout: any;

    public availableCats: any;
    public availableSounds: any;
    availableSoundsArray: number[] = [];
    public usedCats: any;
    public usedsounds: any;
    subs = new Subscription();
    dragElmId = null;

    translations: any = {};

    constructor(public alertController: AlertController,
                private http: HTTP,
                private menu: MenuController,
                private translateService: TranslateService,
                public loadingController: LoadingController,
                private dragulaService: DragulaService,
                public storage: Storage) {

        this.translateService.get(['error', 'noCourse']).subscribe(values => {
            this.translations = values;
        });

        this.load('Загрузка');
        this.color = setInterval(() => {
            this.getColor();
        }, 2000);


        this.subs.add(this.dragulaService.drag('categories')
            .subscribe(({name, el, source}) => {
                this.dragElmId = Number(el.id);
                console.log(Number(el.id))
            })
        );
        this.subs.add(this.dragulaService.drop('categories')
            .subscribe(({name, el, target, source, sibling}) => {
                this.dragElmId = null;
            })
        );
    }

    getColor() {
        this.storage.get('color').then((val) => {
            this.color = val;
        });
    }

    getUser() {
        this.storage.get('user').then((val) => {
            this.user = val;
        });
    }

    getPack() {
        this.storage.get('upack').then((val) => {
            this.upack = val;
        });
    }

    async alert(head, mess) {
        const alert = await this.alertController.create({
            header: head,
            message: mess,
            buttons: ['OK']
        });

        await alert.present();
    }

    async load(mess) {
        const loading = await this.loadingController.create({
            message: mess,
            duration: 1000
        });
        await loading.present();

        const {role, data} = await loading.onDidDismiss();

        console.log('Loading dismissed!');
    }

    toggle(i) {
        if (this.playing[i] && this.osc[i]) {
            this.playing[i] = false;
            this.osc[i].stop(0);
        } else {
            this.playing[i] = true;
            this.osc[i] = this.context.createOscillator();
            this.osc[i].connect(this.context.destination);
            this.osc[i].frequency.value = this.freq[i];
            this.osc[i].start(0);
        }
    }

    updateFreq(newFreq, i) {
        this.freq[i] = newFreq;
        if (this.osc[i]) {
            this.osc[i].frequency.value = this.freq[i];
        }
    }

    getPlayLists() {
        this.http.get('http://sound.aumagency.ru/api/user/getPlayList', {}, {})
            .then(data => {
                this.playlists = JSON.parse(data.data).list;
            })
            .catch(error => {
            });
    }

    getSounds() {
        this.http.get('http://sound.aumagency.ru/api/sound/getSoundList', {}, {})
            .then(data => {
                this.sounds = JSON.parse(data.data).list;
                // this.filtredSounds = JSON.parse(data.data).list;
                this.reload();
                if (this.availableSoundsArray.length !== 0) {
                    this.filtredSounds = this.sounds.filter(sound => {
                        return this.availableSoundsArray.indexOf(sound.id) !== -1;
                    });
                }
                // this.sounds = JSON.parse(data.data).list;
            })
            .catch(error => {
            });
    }

    getSettings() {
        this.http.get('http://sound.aumagency.ru/api/user/getSettings', {}, {})
            .then(data => {
                this.settings = JSON.parse(data.data);
                this.reload();
                this.getSounds();
            })
            .catch(error => {
            });
    }

    getCourses() {
        this.http.get('http://sound.aumagency.ru/api/course/getCourseList', {}, {})
            .then(data => {
                this.courses = JSON.parse(data.data).list;
                console.log(this.courses)
            })
            .catch(error => {
            });
    }

    getCategories() {
        this.http.get('http://sound.aumagency.ru/api/cat/getCateoryList', {}, {})
            .then(data => {
                this.categories = JSON.parse(data.data).list;

                console.log(this.categories, 'this.categories');
            })
            .catch(error => {
            });
    }

    async playCourse(id) {
        if (!this.courses) {
            this.alert(this.translations.error, this.translations.noCourse);
            return false;
        }

        for (const course of this.courses) {
            if (course.id === id) {
                for (const sound of course.sounds) {
                    this.playSound(sound.id);
                    await this.delay(this.getDuration(sound.id));
                    this.stop();
                }
            }
        }
    }

    getDuration(id) {
        this.counter = this.counter ? this.counter : this.duration * 60;
        this.startCountDown();
        return this.duration * 60;

    }

    resetSounds() {
        this.currSound = undefined;
        this.currSoundId = undefined;
        this.duration = undefined;
        this.progress = 0;
        this.counter = 0;
        this.selectedSounds = [];
        this.countDown = of(0);
        if (this.play) {
            this.stop();
            this.play = false;
            clearTimeout(this.progressTimeout);
        }
    }

    choseSound(sound) {
        if (+this.currSoundId === sound.id) {
            this.resetSounds();
            return;
        }
        this.currSound = sound;
        this.currSoundId = sound.id;
        this.duration = sound.duration;
        this.progress = 0;
        this.counter = 0;
        this.countDown = of(0);
        if (this.play) {
            this.stop();
            this.play = false;
            clearTimeout(this.progressTimeout);
        }
    }

    pause() {
        this.stop();
        this.countDown = of(this.counter);
    }

    playSound(id) {
        if (this.play) {
            this.stop();
        } else {
            for (const sound of this.sounds) {
                if (sound.id === id) {
                    this.updateFreq(sound.hz1, 1);
                    this.updateFreq(sound.hz2, 2);
                    this.updateFreq(sound.hz3, 3);
                    this.updateFreq(sound.hz4, 4);
                    this.updateFreq(sound.hz5, 5);
                    this.toggle(1);
                    this.toggle(2);
                    this.toggle(3);
                    this.toggle(4);
                    this.toggle(5);
                    this.currSound = this.currSound ? this.currSound : sound;
                    this.currSoundId = this.currSoundId ? this.currSoundId : sound.id;
                    this.duration = this.duration ? this.duration : sound.duration;
                    this.play = true;
                    this.getDuration(sound.id);
                    this.setProgress(sound.duration);

                }
            }
        }
    }

    changeSelectedSounds(soundId, event) {
        event.stopPropagation();

        if (this.selectedSounds.indexOf(soundId) === -1) {
            if (this.selectedSounds.length >= 5) {
                this.alert('', this.translations.max5);
            } else {
                this.selectedSounds.push(soundId);
            }
        } else {
            const soundIndex = this.selectedSounds.indexOf(soundId);
            this.selectedSounds.splice(soundIndex, 1);
        }
    }

    stop() {
        this.toggle(1);
        this.toggle(2);
        this.toggle(3);
        this.toggle(4);
        this.toggle(5);
        this.play = false;
    }

    delay(ms: number) {
        return new Promise(resolve => setTimeout(resolve, ms * 1000));
    }

    setCourse(id) {
        this.resetSounds();
        if (this.selectedCourse === id) {
            this.selectedCourse = undefined;
            this.soundlist = undefined;
            return;
        }
        this.selectedCourse = id;
        for (const course of this.courses) {
            if (course.id === id) {
                this.soundlist = course.sounds;
            }
        }
    }

    startCountDown() {
        this.countDown = Observable.timer(0, this.tick)
            .take(this.counter)
            .map(() => --this.counter);
    }

    async filterCourses(id) {
        // this.load('Загрузка');
        this.resetSounds();
        if (+id === +this.cat_id) {
            this.cat_id = undefined;
            this.selectedCourse = undefined;
            this.soundlist = undefined;
            return;
        }
        this.cat_id = +id;
        this.soundlist = null;
        this.selectedCourse = null;
        console.log(id, this.cat_id);
        // this.getCourses();
        // await this.delay(2);
        // this.coursesFilter = this.courses.filter((item) => {
        //     if (item.categories[0].id == id) {
        //         return true;
        //     } else {
        //      return false;
        //     }
        // });
    }

    openMenu() {
        this.menu.open('first');
    }

    closeMenu() {
        this.menu.close('first');
    }

    autoPlayPrev() {
        if (this.soundlist.length === 1) {
            return;
        }
        for (let i = 0; i < this.soundlist.length; i++) {
            if (this.soundlist[i].id === this.currSoundId && i === 0) {
                this.choseSound(this.soundlist[this.soundlist.length - 1]);
                this.playSound(this.soundlist[this.soundlist.length - 1].id);
                return;
            } else if (this.soundlist[i].id === this.currSoundId && i !== 0) {
                this.choseSound(this.soundlist[i - 1]);
                this.playSound(this.soundlist[i - 1].id);
                return;
            }

        }
    }


    autoPlayNext() {
        if (this.soundlist.length === 1) {
            return;
        }
        for (let i = 0; i < this.soundlist.length; i++) {
            console.log(this.soundlist, '\n', this.soundlist[i].id, this.currSoundId, i);
            if (this.soundlist[i].id === this.currSoundId && i !== this.soundlist.length - 1) {
                this.choseSound(this.soundlist[i + 1]);
                this.playSound(this.soundlist[i + 1].id);
                return;
            } else if (this.soundlist[i].id === this.currSoundId && i === this.soundlist.length - 1) {
                this.choseSound(this.soundlist[0]);
                this.playSound(this.soundlist[0].id);
                return;
            }

        }
    }

    incresProgress(i, step) {
        if (i === 1) {
            return;
        }
        this.progressTimeout = setTimeout(() => {
            i += step;
            this.progress = i;
            if (!this.play) {
                return;
            }
            if (this.progress < 1) {
                this.incresProgress(i, step);
            } else {
                this.autoPlayNext();
            }
        }, 1000);
    }

    setProgress(soundDuration: any) {
        const step = 1 / (+soundDuration * 60);
        this.incresProgress(this.progress, step);
    }

    ionViewDidEnter() {
        this.upack = this.getPack();
        this.getUser();
        this.getSettings();
        this.getCourses();
        this.getPlayLists();
        this.getSounds();
        this.getCategories();
    }

    getOwnCategoriesId() {
        const categories = [];
        this.categories.filter(category => {
            if (category.user_id === this.user.id) {
                categories.push(`${category.id}`);
            }
        });
        return categories;
    }

    reload() {
        if (!this.settings) {
            this.getSettings();
            return;
        }
        this.availableCats = this.settings[this.upack + '_cats'].value;
        this.availableCats = this.availableCats.split(',').concat(this.getOwnCategoriesId());
        this.availableSounds = this.settings[this.upack + '_sounds'].value;
        this.availableSoundsArray = this.availableSounds.split(',').map(id => +id);

    }

    getItems(ev) {
        const val = ev.target.value;
        if (val && val.trim() != '') {
            this.filtredSounds = this.sounds.filter((item) => {
                return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        } else {
            this.getSounds();
        }
    }

}

@Pipe({name: 'order', pure: false})
export class OrderBy implements PipeTransform {

    static _orderByComparator(a: any, b: any): number {

        if ((isNaN(parseFloat(a)) || !isFinite(a)) || (isNaN(parseFloat(b)) || !isFinite(b))) {
            if (a < b) return -1;
            if (a > b) return 1;
        }
        else {
            //Parse strings as numbers to compare properly
            if (parseFloat(a) < parseFloat(b)) return -1;
            if (parseFloat(a) > parseFloat(b)) return 1;
        }

        return 0; //equal each other
    }

    transform(input: any, [config = '+']): any {

        if (!Array.isArray(input)) return input;

        if (!Array.isArray(config) || (Array.isArray(config) && config.length == 1)) {
            var propertyToCheck: string = !Array.isArray(config) ? config : config[0];
            var desc = propertyToCheck.substr(0, 1) == '-';

            //Basic array
            if (!propertyToCheck || propertyToCheck == '-' || propertyToCheck == '+') {
                return !desc ? input.sort() : input.sort().reverse();
            }
            else {
                var property: string = propertyToCheck.substr(0, 1) == '+' || propertyToCheck.substr(0, 1) == '-'
                    ? propertyToCheck.substr(1)
                    : propertyToCheck;

                return input.sort(function (a: any, b: any) {
                    return !desc
                        ? OrderBy._orderByComparator(a[property], b[property])
                        : -OrderBy._orderByComparator(a[property], b[property]);
                });
            }
        }
        else {
            //Loop over property of the array in order and sort
            return input.sort(function (a: any, b: any) {
                for (var i: number = 0; i < config.length; i++) {
                    var desc = config[i].substr(0, 1) == '-';
                    var property = config[i].substr(0, 1) == '+' || config[i].substr(0, 1) == '-'
                        ? config[i].substr(1)
                        : config[i];

                    var comparison = !desc
                        ? OrderBy._orderByComparator(a[property], b[property])
                        : -OrderBy._orderByComparator(a[property], b[property]);

                    if (comparison != 0) return comparison;
                }

                return 0;
            });
        }
    }
}

@Pipe({
    name: 'formatTime',
})
export class FormatTimePipe implements PipeTransform {

    transform(value: number): string {
        const minutes: number = Math.floor(value / 60);
        return ('00' + minutes).slice(-2) + ':' + ('00' + Math.floor(value - minutes * 60)).slice(-2);
    }

}

