import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
    { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
    {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
    {
    path: 'list',
    loadChildren: './list/list.module#ListPageModule'
  },
  { path: 'register', loadChildren: './register/register.module#RegisterPageModule' },
  { path: 'info', loadChildren: './info/info.module#InfoPageModule' },
  { path: 'player', loadChildren: './player/player.module#PlayerPageModule' },
  { path: 'coupons', loadChildren: './coupons/coupons.module#CouponsPageModule' },
  { path: 'history', loadChildren: './history/history.module#HistoryPageModule' },
  { path: 'unsub', loadChildren: './unsub/unsub.module#UnsubPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
