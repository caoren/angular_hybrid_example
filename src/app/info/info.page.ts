import { Component, OnInit } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { MenuController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import {BehaviorSubject} from 'rxjs';
@Component({
  selector: 'app-info',
  templateUrl: './info.page.html',
  styleUrls: ['./info.page.scss'],
})
export class InfoPage implements OnInit {
    public email: string;
    public name: string;
    public color: string;
    public type = 'asdasda';
    public database: any;
  constructor(public sqlite: SQLite, public menu: MenuController, private storage: Storage) {
      this.sqlite.create({ name: "data.db", location: "default" }).then((db: SQLiteObject) => {
          console.log("opened info");
          this.database = db;
          this.database.executeSql("SELECT * FROM user", []).then((data) => {
              if (data.rows.length > 0) {
                  this.name = data.rows.item(0).name;
                  this.email = data.rows.item(0).email;
              }
          }, (error) => {
              alert("error user: " + JSON.stringify(error.err));
              console.log("ERROR: " + JSON.stringify(error.err));
          });
      }, (error) => {
          console.log("ERROR: ", error);
          alert(error);
      });
      storage.get('color').then((val) => {
          this.color = val;
      });
  }

  ngOnInit() {
      this.menu.swipeEnable(false );
      this.menu.open();
  }

    ionViewWillEnter() {
        this.menu.swipeEnable(false );
        this.menu.open();
    }
    setColor(color) {
      // const storageObserver = new BehaviorSubject(null);
      this.storage.set('color', color);
      // this.storageObserver.next(null);
      this.color = color;
    }

}
