import { Component, OnInit } from '@angular/core';
import {AlertController, MenuController} from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { Router } from '@angular/router';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import {Storage} from '@ionic/storage';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
    public email: string;
    public password: string;
    public database: any;
    public user: any;
    translations: any = {};

    constructor(
      public alertController: AlertController,
      private http: HTTP,
      private router: Router,
      public sqlite: SQLite,
      private menu: MenuController,
      private translateService: TranslateService,
      public storage: Storage) {

        this.translateService.get(['error', 'noUser', 'notAviable']).subscribe(values => {
            this.translations = values;
        });
    }

    changeLang(data) {
        this.translateService.use(data);
        this.storage.set('lang', data);
    }

    tryLogin() {
        if (this.email && this.password) {
            this.http.get('http://sound.aumagency.ru/api/user/login', {'email': this.email, 'password': this.password}, {})
                .then(data => {
                        // console.log(JSON.parse(data.data).code);
                        if (JSON.parse(data.data).code == "500") {
                            this.alert(this.translations.error, JSON.parse(data.data).msg);
                            return false;
                        }
                        // this.router.navigate(['/player']);
                        this.menu.enable(true, 'first');
                        this.user = JSON.parse(data.data).data;
                        this.storage.set('user', this.user);
                        this.saveUser(this.user);
                        this.storage.set('upack', this.user.coupons[0].pack);
                        if (this.user.coupons[0].status == 'active') {
                            this.router.navigate(['/player']);
                            this.menu.enable(true, 'first');
                        } else {
                            this.router.navigate(['/coupons']);
                            this.menu.open('first');
                            // this.router.navigate(['/player']);
                        }
                    // } else {
                    //     this.alert('Ошибка', 'Пользвователь не найден!'); }
                })
                .catch(error => {

                  console.log(error, 'error', this.email && this.password);
                    this.alert(this.translations.error, this.translations.noUser);
                });
        } else {
            this.alert(this.translations.error, this.translations.enterData);
        }
    }

    async alert(head, mess) {
        const alert = await this.alertController.create({
            header: head,
            message: mess,
            buttons: ['OK']
        });

        await alert.present();
    }

    saveUser(u) {
      this.sqlite.create({ name: 'data.db', location: 'default' }).then((db: SQLiteObject) => {
          console.log('opened ');
          this.database = db;
          this.database.executeSql('INSERT INTO user(uid, name, email) VALUES (?, ?, ?)', [String(u['id']), String(u['name']), String(u['email'])]).then((data) => {
            }, (error) => {
                alert('ERROR UPDATE ' + JSON.stringify(error));
            });
        }, (error) => {
            console.log('ERROR: ', error);
            alert(error);
        });
    }

  ngOnInit() {
  }

}
