import { Component, OnInit, TemplateRef} from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import {AlertController, MenuController} from '@ionic/angular';
import { Router } from '@angular/router';
import {SQLite, SQLiteObject} from '@ionic-native/sqlite/ngx';
import {BsModalService, BsModalRef} from 'ngx-bootstrap';
import {TranslateService} from "@ngx-translate/core";
import {Storage} from '@ionic/storage';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  public email: string;
  public password: string;
  public database: any;
  public fname: string;
  public lname: string;
  public code: string;
  public tel: string;
  public repassword: string;
  public address: string;
  modalRef: BsModalRef;
  translations: any = {};
  constructor(
      public alertController: AlertController,
      private http: HTTP,
      private router: Router,
      private menu: MenuController,
      private translateService: TranslateService,
      public sqlite: SQLite,
      public storage: Storage,
      private modalService: BsModalService) {

      this.translateService.get(['error', 'passError', 'tryLater', 'addAllData', 'allOk', 'accRgistered']).subscribe(values => {
          this.translations = values;
      });
  }
    tryRegister() {
        if (this.password !== this.repassword) {
            this.alert(this.translations.error, this.translations.passError);
            return false;
        }
        if (this.email && this.password && this.repassword) {
            let sendData = {
                first_name: this.fname,
                last_name: this.lname,
                address: this.address,
                phone: this.tel,
                email: this.email,
                password: this.password};
            this.http.post('http://sound.aumagency.ru/api/user/userAdd', sendData, {})
                .then(data => {
                    // console.log(JSON.parse(data.data).data);
                    this.saveUser(JSON.parse(data.data).data);
                    this.modalRef = this.modalService.show(ModalContentComponent);
                    this.modalRef.setClass('unsubModal');
                    this.router.navigate(['/coupons']);
                    this.menu.enable(true, 'first');
                    this.menu.open('first');
                    // this.openModal(modal);
                    // this.presentAlertConfirm();
                })
                .catch((error: any) => {
                    console.log(error);
                    this.alert(this.translations.error, this.translations.tryLater);
                });
        } else {
            this.alert(this.translations.error, this.translations.addAllData);
        }
    }
    async alert(head, mess) {
        const alert = await this.alertController.create({
            header: head,
            message: mess,
            buttons: ['OK']
        });

        await alert.present();
    }

    async presentAlertConfirm() {
        const alert = await this.alertController.create({
            header: this.translations.allOk,
            message: this.translations.accRgistered,
            buttons: [
                {
                    text: 'Ок',
                    handler: () => {
                        this.router.navigate(['/coupons']);
                        this.menu.enable(true, 'first');
                        this.menu.open('first');
                    }
                }
            ]
        });

        await alert.present();
    }

    saveUser(u) {
        this.sqlite.create({ name: 'data.db', location: 'default' }).then((db: SQLiteObject) => {
            console.log('opened ');
            this.database = db;
            this.database.executeSql('INSERT INTO user(uid, name, email) VALUES (?, ?, ?)', [String(u['id']), String(u['name']), String(u['email'])]).then((data) => {
            }, (error) => {
                alert('ERROR UPDATE ' + JSON.stringify(error));
            });
        }, (error) => {
            console.log('ERROR: ', error);
            alert(error);
        });
    }

    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
        this.modalRef.setClass('unsubModal');
    }

    ngOnInit() {
    }

    changeLang(data) {
        this.translateService.use(data);
        this.storage.set('lang', data);
    }
}
@Component({
    selector: 'modal-content',
    template: `
        <div class="modal-content">
            <div class="already_unsubed">
                <div class="unsubed_img">
                    <img src="../assets/img/email.png" alt="canc_subs">
                </div>
                <p>{{'invoiceSended' | translate}}</p>
                <a (click)="bsModalRef.hide()">OK</a>
            </div>
        </div>
  `
})
export class ModalContentComponent implements OnInit {
    title: string;
    closeBtnName: string;
    list: any[] = [];

    constructor(public bsModalRef: BsModalRef) {}

    ngOnInit() {
        // this.list.push('PROFIT!!!');
    }
}
