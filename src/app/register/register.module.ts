import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {HTTP} from '@ionic-native/http/ngx';
import {ModalContentComponent, RegisterPage} from './register.page';
import {NgxMaskIonicModule} from 'ngx-mask-ionic';
import {NgxPopperModule} from 'ngx-popper';
import {TranslateModule} from "@ngx-translate/core";
// import {BsModalService, BsModalRef} from 'ngx-bootstrap';
const routes: Routes = [
    {
        path: '',
        component: RegisterPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        TranslateModule,
        NgxMaskIonicModule,
        NgxPopperModule,
        // BsModalRef,
        RouterModule.forChild(routes)
    ],
    declarations: [RegisterPage],
    providers: [HTTP],
    entryComponents: []
})
export class RegisterPageModule {
}
