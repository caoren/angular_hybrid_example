import { Component, OnInit } from '@angular/core';
import { AlertController, MenuController} from '@ionic/angular';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-coupons',
  templateUrl: './coupons.page.html',
  styleUrls: ['./coupons.page.scss'],
})
export class CouponsPage implements OnInit {
  public page = 1;
  public coupone = 0;
  public code = '';
  public term = 0;
  public statusClass = '';

  translations: any = {};

  constructor(public alertController: AlertController, private menu: MenuController, private translateService: TranslateService) {

      this.translateService.get(['error', 'codeError', 'notAviable']).subscribe(values => {
        this.translations = values;
      });
  }
  ngOnInit() {
      setInterval(this.updateStatus, 500);
  }

    setCoupone(id) {
        this.coupone = id;
        this.page = 2;
        this.updateStatus();
    }

    setTerm(term) {
        this.term = term;
        this.page = 3;
        this.updateStatus();
    }

    GoTo(page) {
    this.page = page;
    this.updateStatus();
    }

    checkCode() {
        this.alert(this.translations.error, this.translations.codeError);
    }

    unavailable() {
        this.alert(this.translations.error, this.translations.notAviable);
    }

    async alert(head, mess) {
        const alert = await this.alertController.create({
            header: head,
            message: mess,
            buttons: ['OK']
        });

        await alert.present();
    }

    updateStatus() {
    if (this.page == 1) {
      this.statusClass = ''; } else if (this.page == 2) {
      this.statusClass = 'half_status'; } else if (this.page == 3) {
      this.statusClass = 'more_half_status'; } else if (this.page == 6) {
      this.statusClass = 'full_percents'; }
    }

    toggleMenu() {
        this.menu.toggle('first');
    }

}
